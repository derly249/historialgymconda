from django.contrib import admin

# Register your models here.
from historias.models import *

class ProjectAdmin(admin.ModelAdmin):
    readonly_fields = ('created','updated')
    list_display = ('NombreCompleto','DifPeso','FaseDieta')
    ordering = ('autor','updated') ##ordenar por dos campos o mas 
    search_fields = ('autor__username','FaseDieta','FotosHistorias__FechaCreacion') ##buscar
    date_hierarchy = 'created'
    list_filter = ('Persona',)
##registrar en el panel

admin.site.register(Atleta)
admin.site.register(HistoriaClinica,ProjectAdmin)
admin.site.register(FotosHistoria)
admin.site.register(ReporteComida)
admin.site.register(TitulosComida)
admin.site.register(ItemsComida)
admin.site.register(ReporteRutina)
admin.site.register(TitulosRutina)
admin.site.register(ItemsRutina)

