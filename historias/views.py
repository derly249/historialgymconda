from django.shortcuts import render
from .models import HistoriaClinica
from .models import ReporteRutina
from django.views.generic.edit import CreateView

# Create your views here.
def home(request):
      historiaclinica = HistoriaClinica.objects.all()
      return render(request, "historias/home.html",  {'historiaclinica': historiaclinica})
       #return HttpResponse("<h1>Victor Personal Trainer</h1><h2>Portada</h2>")
def bienvenida(request):
      return render(request, "historias/bienvenida.html")
      
def wizard(request):
      return render(request, "historias/wizard.html")

def formularioUno(request):
      return render(request, "historias/formularioUno.html")
## Create con clases view
class PageRutina(CreateView):
      model = ReporteRutina
      fields = ['Persona','Titulo']
