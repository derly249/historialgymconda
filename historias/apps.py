from django.apps import AppConfig


class HistoriasConfig(AppConfig):
    name = 'historias'
    verbose_name = 'historiaVN'
    ##para canbiar el verbose name de apps en seting agregarle historias.apps.HistoriasConfig