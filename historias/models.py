from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
# Create your models here.

# Create your models here.

class Atleta(models.Model):
    ApellidoPaterno = models.CharField(max_length=35)
    ApellidoMaterno = models.CharField(max_length=35)
    Nombres = models.CharField(max_length=35)
    CorreoElectronico = models.CharField(max_length=35)
    NumTelefono = models.CharField(max_length=35)
    DNI = models.CharField(max_length=8)
    Direccion = models.CharField(max_length=35)
    Edad = models.CharField(max_length=35)
    FechaNacimiento = models.CharField(max_length=35)
    Estatura = models.CharField(max_length=35)
    PesoActual = models.CharField(max_length=35)
    FotosPerfil = models.ImageField()

    def NombreCompleto(self):
        cadena ="{0} {1}, {2}"
        return cadena.format(self.Nombres,self.ApellidoPaterno,self.ApellidoMaterno)
    
    def __str__(self):
        return self.NombreCompleto()

class FotosHistoria(models.Model):
    upload = models.ImageField(upload_to ='uploads /%Y/%m/%d/')
    FechaCreacion = models.DateTimeField(auto_now_add=True)

    # file will be saved to MEDIA_ROOT / uploads / 2015 / 01 / 30 


#HistoriaClinica
class HistoriaClinica(models.Model):
    Persona = models.ForeignKey(Atleta,blank=True, on_delete=models.CASCADE)
    PesoInicial = models.FloatField()
    PesoActual = models.FloatField()
    DifPeso = models.FloatField(verbose_name="Diferencia de peso")
    SensacionEntrenamiento = models.CharField(max_length=650)
    SensacionNutricion = models.CharField(max_length=650)
    SensacionRecuperacion = models.CharField(max_length=650)
    TerapiaErgogenica = models.CharField(max_length=650)
    TerapiaPCT = models.CharField(max_length=650)
    EjercicioCardioVascular = models.CharField(max_length=650)
    Entrenamiento = models.CharField(max_length=650)
    FaseDieta = models.CharField(max_length=650)
    PreCompeticion = models.CharField(max_length=650)
    FotosHistorias = models.ManyToManyField(FotosHistoria, verbose_name = "Fotos Historico")

    #upload = models.ImageField(upload_to ='uploads /%Y/%m/%d/')
    #FotosHistorias = models.ForeignKey(FotosHistoria,blank=True, on_delete=models.CASCADE)
    #FechaCreacion = models.DateTimeField(auto_now_add=True)
    #FechaModificacion = models.DateTimeField(auto_now =True)
    autor = models.ForeignKey(User,blank=True,null=True,on_delete=models.PROTECT, verbose_name = "Autor de Creacion")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creacion")
    updated = models.DateTimeField(auto_now=True,verbose_name="Fecha de modificacion")

    def NombreCompleto(self):
        cadena ="{0} , {1} {2}"
        return cadena.format(self.Persona.Nombres,self.Persona.ApellidoPaterno,self.Persona.ApellidoMaterno)
    
    def __str__(self):
        return self.NombreCompleto()

#Reporte Comidas
class ItemsComida(models.Model):
    NomItem = models.CharField(max_length=350)
    def __str__(self):
        return self.NomItem

class TitulosComida(models.Model):
    NomTitulo = models.CharField(max_length=350)
    FechaCreacion = models.DateTimeField(auto_now_add=True)
    Items = models.ManyToManyField(ItemsComida)

    def __str__(self):
        return self.NomTitulo

class ReporteComida(models.Model):
    Persona = models.ForeignKey(Atleta,blank=True, on_delete=models.CASCADE)
    FechaCreacion = models.DateTimeField(auto_now_add=True)
    Titulo = models.ManyToManyField(TitulosComida,blank=True)
    def Cabecera(self):
        cadena ="{0} , {1} {2} - {3}"
        return cadena.format(self.Persona.Nombres,self.Persona.ApellidoPaterno,self.Persona.ApellidoMaterno,self.FechaCreacion) 
    
    def __str__(self):
        return self.Cabecera()

#Reporte Ejercicios
class ItemsRutina(models.Model):
    NomItem = models.CharField(max_length=350)
    def __str__(self):
        return self.NomItem

class TitulosRutina(models.Model):
    NomTitulo = models.CharField(max_length=350)
    FechaCreacion = models.DateTimeField(auto_now_add=True)
    Items = models.ForeignKey(ItemsRutina,blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.NomTitulo

class ReporteRutina(models.Model):
    Persona = models.ForeignKey(Atleta,blank=True, on_delete=models.CASCADE)
    FechaCreacion = models.DateTimeField(auto_now_add=True)
    Titulo = models.ManyToManyField(TitulosRutina,blank=True)
    def Cabecera(self):
        cadena ="{0} , {1} {2} - {3}"
        return cadena.format(self.Persona.Nombres,self.Persona.ApellidoPaterno,self.Persona.ApellidoMaterno,self.FechaCreacion) 
    
    def __str__(self):
        return self.Cabecera()

